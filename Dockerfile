FROM node:12.14-alpine

WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH

COPY package.json ./
COPY yarn.lock ./
RUN yarn install \
    && yarn add global react-scripts@3.4.1

COPY . ./


CMD ["yarn", "start"]