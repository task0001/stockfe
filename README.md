## Run the application

As the main goal of the task was to create the backend, in this "application" I am just showing the list of Stocks and the use of some react hooks.

Only the delete button was implemented.

1 - Build the application
```
yarn install
```

2 - Run Spring Boot from maven
```
yarn start
```

## Run from docker
For working properly make sure the stock BE is running
```
docker-compose -f ./docker-compose.yml up --build -d
```