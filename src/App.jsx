import './App.css';
import TaskTable from './components/TaskTable';

function App() {
  return (
    <div className="App">
      <header>
        Assignment Task
      </header>
      <TaskTable />
    </div>
  );
}

export default App;
