
export const contextPath = process.env.REACT_APP_API_CONTEXT_PATH || '';
export const apiPath = `${contextPath}/payconiq`;
