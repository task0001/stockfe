import React from 'react';
import { TableHead, TableRow, TableCell } from '@material-ui/core';

export default function TableHeader({ columns }) {
  return (
    <TableHead>
      <TableRow>
        <TableCell>
          <div>
            <span>Stock ID</span>
          </div>
        </TableCell>
        <TableCell>
          <div>
            <span>Stock Name</span>
          </div>
        </TableCell>
        <TableCell>
          <div>
            <span>Actions</span>
          </div>
        </TableCell>
      </TableRow>
    </TableHead>
  );
}
