import React from 'react';
import {
  Tooltip,
  TableRow,
  TableCell,
  TableBody,
} from '@material-ui/core';
import MuiIconButton from '@material-ui/core/IconButton';

import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';

export default function FOTableBody({ data, onDelete }) {
  return (
    <TableBody>
      {data?.length ? (
        data.map((stock) => {
          return (
            <TableRow key={stock.id}>
              <TableCell >
                <span>{stock.id}</span>
              </TableCell>
              <TableCell>
                <span>{stock.name}</span>
              </TableCell>
              <TableCell>
                <div>
                  <Tooltip
                    title='Edit'
                  >
                    <span>
                      <MuiIconButton
                        color="secondary"
                        size="small"
                        onClick={console.log('edit click')}
                      >
                        <EditIcon />
                      </MuiIconButton>
                    </span>
                  </Tooltip>
                  <Tooltip
                    title='Delete'
                  >
                    <span>
                      <MuiIconButton
                        color="secondary"
                        size="small"
                        onClick={onDelete(stock)}
                      >
                        <DeleteIcon />
                      </MuiIconButton>
                    </span>
                  </Tooltip>
                </div>
              </TableCell>
            </TableRow>
          );
        })
      ) : (
        <TableRow>
          <TableCell colSpan={12} align={'center'}>
            <span>No data to show</span>
          </TableCell>
        </TableRow>
      )}
    </TableBody>
  );
}
