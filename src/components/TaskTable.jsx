import React from 'react';
import { CircularProgress, Table } from '@material-ui/core';
import TableHeader from './TableHeader/TableHeader';
import TableBody from './TableBody/TableBody';
import useTable from './useTable';

export default function TaskTable() {
  const { loading, data, onDelete } = useTable();
  return (
    <>
      {loading ? (
        <CircularProgress size={24} />
      ) : (
        <div style={{ overflowX: 'auto' }}>
          <Table size="small">
            <TableHeader/>
            <TableBody data={data} onDelete={onDelete} />
          </Table>
        </div>
      )}
    </>
  );
}