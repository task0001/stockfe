import { useCallback, useEffect, useState } from 'react';
import axios from 'axios';

export const PAGE_SIZES = [
  10,
  15,
  20,
  25,
  50,
  100,
];

export default function useTable() {
  const [data, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [page, setPage] = useState(0);
  const [size, setSize] = useState(25);

  const onDelete = row =>async (event) => {
    try {
      await axios.delete('http://localhost:5003/payconiq/stock/'.concat(row.id));
      loadData();
    } catch (err) {
      console.log('Error deleting stock: ' + err);
    }
  };

  const loadData = useCallback(async () => {
    setLoading(true);
    let response;
    try {
      response = await axios.get('http://localhost:5003/payconiq/stock', {
        params: {
          page: page,
          size:size,
        }
      });
      setData(response.data);
      setLoading(false);
    } catch (err) {
      setLoading(false);
    }
  }, [
    data,
    page,
    size,
    setData,
  ]);

  useEffect(async () => {
    loadData();
  }, []);

  return {
    data,
    loading,
    page,
    size,
    setPage,
    setSize,
    onDelete,
  }
}
